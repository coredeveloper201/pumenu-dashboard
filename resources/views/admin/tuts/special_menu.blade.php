<p>
	<small style="margin-top: 10px; font-size: 14px;">
		Note : About Special Menu section<br>
		Special menu will show under specific category in your restaurant landing page. <br>
		You can add, edit, delete your special menu item. You need to associated categories with your special menu item.
	</small>
</p>