@push('style_stack')
<style>
  .rating {
    width: 120px;
  }
  .rating i {
    color: #C59D5F;
  }
</style>
@endpush
@extends('admin.partials.mainLayout')
@section('content')
  <div ui-view class="app-body" id="view">
    <div class="padding">
      <div class="box">
        <div class="table-responsive">
          <table class="table table-striped b-t b-b" id="tblData">
            <thead>
            <tr>
              <th style="">reviewer</th>
              <th style="">title</th>
              <th style="">review</th>
              <th style="">rating</th>
              <th style="">Status</th>
            </tr>
            </thead>
            <tbody>
              @foreach($surveys as $survey)
                <tr>
                  <td>{{$survey->reviewer_name}}</td>
                  <td>{{$survey->title}}</td>
                  <td>{{$survey->content}}</td>
                  <td class="rating">
                    @for($i = 0; $i < $survey->rating; $i++)
                      <i class="fa fa-star"></i>
                    @endfor
                  </td>
                  <td>
                    <form action="{{action('SurveyController@survey_approve')}}" method="POST">
                        @csrf
                        <input type='hidden' name='id' value='{{$survey->id}}'/>
                        @if($survey->status == 1) 
                          <button class="btn btn-success" type="submit">Approved</button>
                        @else
                          <button class="btn btn-danger" type="submit">Disapproved</button>
                        @endif
                    </form>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection()