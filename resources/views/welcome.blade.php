<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <link rel="icon" href="{{ asset('front/images/favicon.ico') }}">
  <title>Pumenu.com Resturent Locate</title>
  <!-- LayerSlider CSS -->
  <link rel="stylesheet" href="{{ asset('admin-assets/bootstrap/dist/css/bootstrap.min.css?'.time()) }}" type="text/css"/>
  <link rel="stylesheet" href="{{ asset('slider/layerslider/css/layerslider.css') }}">

  <!-- Google Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900,400|Nunito:300,regular,200,600|Caveat:regular">

  {{--<link type="text/css" rel="stylesheet" href="http://innovastudio.com/builderdemo/assets/minimalist-basic/content.css"/>--}}

  <link type="text/css" rel="stylesheet" href="{{ asset('front/css/reset.css')  }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('front/css/plugins.css')  }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('front/css/style.css')  }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('front/css/color.css')  }}">

<!--   <link href="{{ asset('contentbuilder/assets/minimalist-basic/content-foundation.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('contentbuilder/contentbuilder/contentbuilder.css') }}" rel="stylesheet" type="text/css"/> -->
  <!-- External libraries: jQuery & GreenSock -->
  <link rel="stylesheet" href="{{ asset('admin-assets/font-awesome/css/font-awesome.min.css?'.time()) }}" type="text/css"/>
  <script src="{{ asset('slider/layerslider/js/jquery.js') }}"></script>
  <script src="{{ asset('slider/layerslider/js/greensock.js') }}"></script>

  <!-- LayerSlider script files -->
  <script src="{{ asset('slider/layerslider/js/layerslider.transitions.js') }}"></script>
  <script src="{{ asset('slider/layerslider/js/layerslider.kreaturamedia.jquery.js') }}"></script>
  <style>
    hr {
      max-width: 100%;
    }

    body { margin: 0px !important}

    .btnClicker {
      background-color: rgb(159, 5, 7) !important;
    }

    .row {
      /*max-width: 80.5rem !important;*/
    }

    /*auto complete */
    .autocomplete {
      /*the container must be positioned relative:*/
      position: relative;
      display: inline-block;
    }

    input {
      border: 1px solid transparent;
      background-color: #f1f1f1;
      padding: 10px;
      font-size: 16px;
    }

    input[type=text] {
      background-color: #f1f1f1;
      width: 100%;
    }

    input[type=submit] {
      background-color: DodgerBlue;
      color: #fff;
    }

    .autocomplete-items {
      position: absolute;
      border: 1px solid #d4d4d4;
      border-bottom: none;
      border-top: none;
      z-index: 99;
      /*position the autocomplete items to be the same width as the container:*/
      top: 100%;
      left: 0;
      right: 0;
    }

    .autocomplete-items div {
      padding: 10px;
      cursor: pointer;
      background-color: #fff;
      border-bottom: 1px solid #d4d4d4;
    }

    .autocomplete-items div:hover {
      /*when hovering an item:*/
      background-color: #e9e9e9;
    }

    .autocomplete-active {
      /*when navigating through the items using the arrow keys:*/
      background-color: DodgerBlue !important;
      color: #ffffff;
    }

    .outercontainer {
      /*text-align: center !important;*/
    }

    .autocomplete {
      width: 45%;
    }
  </style>
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div class="language-changer">
  <p><a href="<?= (@end(explode('@', request()->route()->getAction('controller'))) == 'catmenu') ? action('frontEndController@language', ['lan' => 'en']) : '#'; ?>" class="en"><img src="{{ asset('img/en.png') }}" alt="Language icon is not available"></a></p>
  <p>
    <a href="<?= (@end(explode('@', request()->route()->getAction('controller'))) == 'catmenu') ? action('frontEndController@language', ['lan' => 'he']) : '#'; ?>" class="il"><img src="{{ asset('img/il.png') }}" alt="Language icon is not available"></a>
  </p>
</div>
<!-- Slider HTML markup -->
<div id="slider" style="display:block;width:1000px;max-height:750px;height:900px;margin:0 auto;margin-bottom: 0px;">

  <!-- Slide 1-->
  <div class="ls-slide" data-ls="kenburnsscale:1.2; parallaxtype:3d;">
    <div style="background: rgb(14,28,49); background: -moz-radial-gradient(center, ellipse cover, rgba(14,28,49,1) 20%, rgba(54,76,108,1) 100%); background: -webkit-radial-gradient(center, ellipse cover, rgba(14,28,49,1) 20%,rgba(54,76,108,1) 100%); background: radial-gradient(ellipse at center, rgba(14,28,49,1) 20%,rgba(54,76,108,1) 100%);top:0px; left:0px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; width:100%; height:100%;" class="ls-l" data-ls="showinfo:1; durationin:500;"></div>
    <div style="top:0px; left:0px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; width:100%; height:100%; background:#000000; opacity:.35;" class="ls-l" data-ls="showinfo:1; durationin:500;"></div>
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-1.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:11px; left:-141px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.25;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:600; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.15; scaleyin:.15; parallax:true; parallaxlevel:-4; rotation:-31; scaleX:.15; 
    scaleY:.15; 
    skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-9.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:130px; left:330px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.15;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; delayin:300; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.2; scaleyin:.2; parallax:true; parallaxlevel:4; rotation:31; 
    scaleX:
    .2; scaleY:
    .2; 
    skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-4.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:250px; left:460px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.85;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; delayin:400; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.45; scaleyin:.45; parallax:true; parallaxlevel:10; rotation:31; 
    scaleX:.45; 
    scaleY:
    .45; skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-6.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:179px; left:-83px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; opacity:.35; border-radius:15px;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:500; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.25; scaleyin:.25; parallax:true; parallaxlevel:-6; rotation:-31; scaleX:.25; 
    scaleY:.25; 
    skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-7.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:60px; left:524px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.45;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; delayin:600; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.25; scaleyin:.25; parallax:true; parallaxlevel:6; rotation:31; 
    scaleX:
    .25; scaleY:
    .25;
     skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-3.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:306px; left:150px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; style:box-shadow: 0 0 100px rgba(0,0,0,0.5); ;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:400; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.55; scaleyin:.55; loop:true; 
    loopoffsetx:-30; 
    loopduration:4950; loopeasing:easeInOutQuad; loopcount:-1; loopyoyo:true; parallax:true; parallaxlevel:-4; rotation:-31; scaleX:.55; scaleY:.55; skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-3.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:2px; left:362px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; style:box-shadow: 0 0 100px rgba(0,0,0,0.5); ; opacity:.15;" data-ls="showinfo:1; offsetxin:600; offsetyin:400; durationin:2000; delayin:700; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.15; scaleyin:.15; 
    loop:true; 
    loopoffsetx:-40; loopduration:3870; loopeasing:easeInOutQuad; loopcount:-1; loopyoyo:true; parallax:true; parallaxlevel:2; rotation:31; scaleX:.15; scaleY:.15; skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-4.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:51px; left:30px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.1;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:700; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.1; scaleyin:.1; loop:true; loopoffsetx:-40; loopduration:4130; 
    loopeasing:easeInOutQuad; 
    loopcount:-1; loopyoyo:true; parallax:true; parallaxlevel:-2; rotation:-31; scaleX:.1; scaleY:.1; skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-5.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:250px; left:-40px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.85;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:300; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.4; scaleyin:.4; parallax:true; parallaxlevel:-10; rotation:-31; scaleX:.4; 
    scaleY:
    .4; 
    skewX:15; 
    skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-2.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:60px; left:665px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.85;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; delayin:200; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.4; scaleyin:.4; parallax:true; parallaxlevel:8; rotation:31; 
    scaleX:.4;
     scaleY:.4; 
    skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-6.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:218px; left:660px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.65; scaleyin:.65; loop:true; loopoffsetx:30; loopduration:4000; loopeasing:easeInOutQuad; 
    loopcount:-1; 
    loopyoyo:true; parallax:true; parallaxlevel:20; rotation:31; scaleX:.65; scaleY:.65; skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-2.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:90px; left:-282px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px; opacity:.65;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:300; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.4; scaleyin:.4; loop:true; loopoffsetx:-25; loopduration:3750; 
    loopeasing:easeInOutQuad; 
    loopcount:-1; loopyoyo:true; parallax:true; parallaxlevel:-8; rotation:-31; scaleX:.4; scaleY:.4; skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-7.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:240px; left:-270px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:100; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.65; scaleyin:.65; loop:true; loopoffsetx:30; loopduration:4300; loopeasing:easeInOutQuad; 
    loopcount:-1; 
    loopyoyo:true; parallax:true; parallaxlevel:-20; rotation:-31; scaleX:.65; scaleY:.65; skewX:15; skewY:-5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-1.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1); ;top:-67px; left:719px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px;" data-ls="showinfo:1; offsetxin:300; offsetyin:200; durationin:2000; delayin:500; easingin:easeOutQuint; fadein:false; rotatein:31; skewxin:-15; skewyin:5; scalexin:.45; scaleyin:.45; loop:true; loopoffsetx:-40; loopduration:4500; 
    loopeasing:easeInOutQuad; 
    loopcount:-1; loopyoyo:true; parallax:true; parallaxlevel:16; rotation:31; scaleX:.45; scaleY:.45; skewX:-15; skewY:5;">
    <img width="600" height="338" src="{{ asset('slider/images/feature-templates-9.jpg') }}" class="ls-l" alt="" style="box-shadow: 0px 50px 100px rgba(2,11,25,1);top:-60px; left:-319px; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; border-radius:15px;" data-ls="showinfo:1; offsetxin:-300; offsetyin:200; durationin:2000; delayin:200; easingin:easeOutQuint; rotatein:-31; skewxin:15; skewyin:-5; scalexin:.45; scaleyin:.45; loop:true; loopoffsetx:-30; loopduration:4320; loopeasing:easeInOutQuad; 
    loopcount:-1; 
    loopyoyo:true; parallax:true; parallaxlevel:-16; rotation:-31; scaleX:.45; scaleY:.45; skewX:15; skewY:-5;">
    <h1 style="text-shadow: 0 10px 20px rgba(0,0,0,.95);top:233px; left:50%; text-align:initial; font-weight:300; font-style:normal; text-decoration:none; font-family:Nunito; color:#ffffff; font-size:30px;" class="ls-l"
        data-ls="showinfo:1; offsetyin:-100lh; durationin:1500; delayin:1000; clipin:100% 0 0 0; texttransitionin:true; texttypein:chars_rand; textshiftin:0; textdurationin:2000; texteasingin:easeOutQuint; textstartatin:transitioninstart + 0; textrotatein:random(-180,180); texttransformoriginin:50% -0% 0;">
      <?php array_walk($tags, function($k, $v){ echo (trim($k[ 'key' ]) == 'subheading') ? $k[ 'value' ] : ''; }) ?>
    </h1>
    <p style="top:160px; left:50%; text-align:initial; font-weight:400; font-style:normal; text-decoration:none; color:rgb(235, 235, 235); font-family:Lato; font-size:3em;" class="ls-l" data-ls="showinfo:1; durationin:5000; delayin:2000; easingin:easeOutElastic; rotatexin:90; transformoriginin:50% 84% 0; loop:true; loopduration:2000; loopstartat:transitioninstart + 0; loopeasing:easeInOutBack; looprotatey:-720; loopcount:-1; looprepeatdelay:5000; loopyoyo:true;"><?php array_walk($tags, function($k, $v){ echo (trim($k[ 'key' ]) == 'heading') ? $k[ 'value' ] : ''; }) ?></p>
    <p style="text-shadow: 0 10px 30px rgba(0,0,0,.5);top:34px; text-align:center; font-weight:400; font-style:normal; text-decoration:none; color:#5ce1ff; font-family:Caveat; font-size:48px; line-height:44px; left:50%;" class="ls-l"
       data-ls="showinfo:1; transitionin:false; texttransitionin:true; texttypein:chars_rand; textshiftin:30; textoffsetxin:random(-100,100); textoffsetyin:random(-100,100); textdurationin:2000; texteasingin:easeOutElastic; textstartatin:transitioninend + 2500; textrotatein:random(-45,45); rotation:-3;"
       data-langen="<?= array_walk($tags, function($k, $v){ echo (trim($k[ 'key' ]) == 'title') ? $k[ 'value' ] : ''; }) ?>" data-langhe="<?= array_walk($tags, function($k, $v){ echo (trim($k[ 'key' ]) == 'title_il') ? $k[ 'value' ] : ''; }) ?>"
    ><?php array_walk($tags, function($k, $v){ echo (trim($k[ 'key' ]) == 'title') ? $k[ 'value' ] : ''; })  ?></p>

    {{-- <a style="display: none" class="ls-l" href="https://layerslider.kreaturamedia.com/" target="_blank" data-ls="showinfo:1; offsetxin:50; durationin:2500; delayin:3000; easingin:easeOutQuint; rotatexin:-60; rotateyin:40; transformoriginin:97.3% 92.1% 0; texttransitionin:true; texttypein:chars_asc; textoffsetxin:-50; textstartatin:transitioninstart - 500; textrotatein:-50; texttransformoriginin:200% 100% 0; hover:true; hoveroffsety:5; hoverdurationin:750; hoveropacity:1; hoverrotatex:20; 
     hoverscalex:1.2;
      hoverscaley:1.2;">
       <p style="text-align:center;text-transform: uppercase; box-shadow: 0px 20px 40px rgba(2,11,25,.5);top:335px; left:382px; text-align:initial; font-weight:600; font-style:normal; text-decoration:none; background:#ff3874; padding-top:8px; padding-bottom:8px; padding-right:22px; padding-left:22px; border-radius:10px; font-family:Helvetica; font-size:24px; color:#ffffff; padding-left: 30px; padding-right: 30px; " class="btnClicker ">Find Resturent</p>
     </a>--}}
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12" style="width: 100%; margin: 0 auto; margin-top: 30px; margin-bottom: 30px; display: none">
      <select class="select2 form-control" name="hotels" style="width: 100%;display: block;;">
        <?php if(isset($data) && count($data) > 0 ){ ?>
        <option value="">Please select a Resturent</option>
        <?php foreach($data as $d){ ?>
        <option value="<?= action('frontEndController@index', ['id' => $d->name]) ?>"><?= $d->name ?></option>
        <?php }
        } ?>
      </select>
    </div>
    <div class="col-lg-12" style="width: 100%; max-width: 100%; margin: 0 auto; margin-top: 30px; margin-bottom: 30px; ">
      <form autocomplete="off" action="#" style="text-align: center;    width: 100%; display: block;">
        <div class="autocomplete" style="">
          <input id="myInput" type="text" name="myCountry" placeholder="Please type Resturent name">
        </div>
        <input type="button" value="Search" class="btn btn-primary" id="btnSearch" style="margin-top: -4px;background: #c59d5f; border: 1px solid #b89256;"/>
      </form>
    </div>
  </div>
<!-- <?//= (isset($content->value)) ? ($content->value) : '';?> -->

  <div style="" class="row">
    <div class="content">
      <!--=============== About ===============-->
      <section class="about-section" id="sec2">
        <div class="container">
          <div class="row">
            <!--about text-->
            <div class="col-md-6">
              <div class="section-title">
                <h3 data-langen="<?= (isset($meta[ 'headingOne' ][ 'value' ])) ? $meta[ 'headingOne' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'headingOne_il' ][ 'value' ])) ? $meta[ 'headingOne_il' ][ 'value' ] : ''; ?>">
                  <?= (isset($meta[ 'headingOne' ][ 'value' ])) ? $meta[ 'headingOne' ][ 'value' ] : ''; ?>
                </h3>
                <h4 class="decor-title">Our story</h4>
                <div class="separator color-separator"></div>
              </div>
              <p data-langen="<?= (isset($meta[ 'descOne' ][ 'value' ])) ? $meta[ 'descOne' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'descOne_il' ][ 'value' ])) ? $meta[ 'descOne_il' ][ 'value' ] : ''; ?>">
                <?= (isset($meta[ 'descOne' ][ 'value' ])) ? $meta[ 'descOne' ][ 'value' ] : ''; ?>
              </p>

            </div>
            <!-- about images-->
            <div class="col-md-6">
              <div class="single-slider-holder">
                <div class="customNavigation">
                  <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                  <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                </div>
                <div class="single-slider owl-carousel">
                  <!-- 1 -->
                  <div class="item">
                    <img src="{{ asset('images/bg/1.jpg') }}" class="respimg" alt="">
                  </div>
                  <!-- 2 -->
                  <div class="item">
                    <img src="{{ asset('images/bg/1.jpg') }}" class="respimg" alt="">
                  </div>
                  <!-- 3 -->
                  <div class="item">
                    <img src="{{ asset('images/bg/1.jpg') }}" class="respimg" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--=============== Opening Hours ===============-->

      <!--section end-->
      <!--=============== About 2 ===============-->
      <section class="about-section">
        <!-- triangle decoration-->
        <div class="triangle-decor">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="single-slider-holder">
                <div class="customNavigation">
                  <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                  <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                </div>
                <div class="single-slider owl-carousel">
                  <!-- 1 -->
                  <div class="item">
                    <img src="{{ asset('images/slider/1.jpg') }}" class="respimg" alt="">
                  </div>
                  <!-- 2 -->
                  <div class="item">
                    <img src="{{ asset('images/slider/1.jpg') }}" class="respimg" alt="">
                  </div>
                  <!-- 3 -->
                  <div class="item">
                    <img src="{{ asset('images/slider/1.jpg') }}" class="respimg" alt="">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="section-title">
                <h3 data-langen="<?= (isset($meta[ 'headingTwo' ][ 'value' ])) ? $meta[ 'headingTwo' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'headingTwo_il' ][ 'value' ])) ? $meta[ 'headingTwo_il' ][ 'value' ] : ''; ?>">
                  <?= (isset($meta[ 'headingTwo' ][ 'value' ])) ? $meta[ 'headingTwo' ][ 'value' ] : ''; ?>
                </h3>
                <h4 class="decor-title">Natoque penatibus</h4>
                <div class="separator color-separator"></div>
              </div>
              <p data-langen="<?= (isset($meta[ 'descTwo' ][ 'value' ])) ? $meta[ 'descTwo' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'descTwo_il' ][ 'value' ])) ? $meta[ 'descTwo_il' ][ 'value' ] : ''; ?>">
                <?= (isset($meta[ 'descTwo' ][ 'value' ])) ? $meta[ 'descTwo' ][ 'value' ] : ''; ?>
              </p>
            </div>
          </div>
        </div>
      </section>
      <!--section end-->
      <!--=============== Weekly Deals ===============-->

      <!--section end-->
      <!--=============== team ===============-->

      <!--section end-->
      <!--=============== menu ===============-->

      <!--section end-->

      <!--=============== parallax section  ===============-->

      <!--section end-->
      <!--=============== reservation ===============-->

      <!--section end-->
      <section class="parallax-section" id="sec6">
        <div class="bg bg-parallax" style="background-image:url(images/bg/1.jpg)" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
        <div class="overlay"></div>
        <div class="container">
          <h2>Our contacts</h2>
          <h3>Were to find us</h3>
        </div>
      </section>
      <section>
        <div class="triangle-decor"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="contact-details">
                <h3 data-langen="<?= (isset($meta[ 'headingOne' ][ 'value' ])) ? $meta[ 'headingOne' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'headingOne_il' ][ 'value' ])) ? $meta[ 'headingOne_il' ][ 'value' ] : ''; ?>">
                  <?= (isset($meta[ 'headingOne' ][ 'value' ])) ? $meta[ 'headingOne' ][ 'value' ] : ''; ?>
                </h3>

                <p data-langen="<?= (isset($meta[ 'contact' ][ 'value' ])) ? $meta[ 'contact' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'contact_il' ][ 'value' ])) ? $meta[ 'contact_il' ][ 'value' ] : ''; ?>">
                  <?= (isset($meta[ 'contact' ][ 'value' ])) ? $meta[ 'contact' ][ 'value' ] : ''; ?>
                </p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="contact-details">
                <h4>Pumenu</h4>
                <ul>
                  <li>
                    <a href="#" data-langen="<?= (isset($meta[ 'address' ][ 'value' ])) ? $meta[ 'address' ][ 'value' ] : ''; ?>" data-langhe="<?= (isset($meta[ 'address_il' ][ 'value' ])) ? $meta[ 'address_il' ][ 'value' ] : ''; ?>">
                      <?= (isset($meta[ 'address' ][ 'value' ])) ? $meta[ 'address' ][ 'value' ] : ''; ?>
                    </a>
                  </li>
                  <li><a href="#"><?= (isset($meta[ 'phone' ][ 'value' ])) ? $meta[ 'phone' ][ 'value' ] : '' ?></a></li>
                  <li><a href="#"><?= (isset($meta[ 'email' ][ 'value' ])) ? $meta[ 'email' ][ 'value' ] : '' ?></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="bold-separator">
            <span></span>
          </div>

        </div>
      </section>
      <section class="no-padding">
        <div class="map-box">
          <div class="map-holder" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);">
            <div id="map-canvas"></div>
          </div>
        </div>
      </section>
      <section>
        <div class="triangle-decor"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="section-title">
                <h3>Get in Touch</h3>
                <h4 class="decor-title">Write us</h4>
                <div class="separator color-separator"></div>
              </div>
              <div class="contact-form-holder">
                <div id="contact-form">
                  <div id="message2"></div>
                  <form method="post" action="{{ action('frontEndController@sendMail') }}" id="contactform">
                    @csrf
                    <input name="name" type="text" class="name" onClick="this.select()" placeholder="Name">
                    <input name="email" type="text" class="email" onClick="this.select()" placeholder="E-mail">
                    <input name="phone" type="text" class="phone" onClick="this.select()" placeholder="Phone">
                    <textarea name="comments" class="comments" onClick="this.select()">Message</textarea>
                    <button type="submit" id="submit">Send</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--=============== testimonials ===============-->
      <section class="parallax-section" style="max-height: 350px;">
        <div class="bg bg-parallax" style="background-image:url({{ asset('images/bg/1.jpg') }})" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
        <div class="overlay"></div>
        <div class="container">
          <h2>Testimonials</h2>
          <h3>What said about us</h3>
          <div class="bold-separator">
            <span></span>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="testimonials-holder">
                <div class="customNavigation">
                  <a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
                  <a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
                </div>
                <div class="testimonials-slider owl-carousel">
                  <div class="item">
                    <ul>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                    </ul>
                    <p>" Numerous commentators have also referred to the supposed restaurant owner's eccentric habit of touting for custom outside his establishment, dressed in aristocratic fashion and brandishing a sword "</p>
                    <h4><a href="https://twitter.com/katokli3mmm" target="_blank">Jone Doe</a></h4>
                  </div>
                  <div class="item">
                    <ul>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star-half"></i></li>
                    </ul>
                    <p>" Numerous commentators have also referred to the supposed restaurant owner's eccentric habit of touting for custom outside his establishment, dressed in aristocratic fashion and brandishing a sword "</p>
                    <h4><a href="https://twitter.com/katokli3mmm" target="_blank">Jone Doe</a></h4>
                  </div>
                  <div class="item">
                    <ul>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                      <li><i class="fa fa-star"></i></li>
                    </ul>
                    <p>" Numerous commentators have also referred to the supposed restaurant owner's eccentric habit of touting for custom outside his establishment, dressed in aristocratic fashion and brandishing a sword "</p>
                    <h4><a href="https://twitter.com/katokli3mmm" target="_blank">Jone Doe</a></h4>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="section-icon"><i class="fa fa-quote-left"></i></div>
        </div>
      </section>
      <!--section end-->
    </div>
    <footer>
      <div class="footer-inner">
        <div class="container">
          <div class="row">
            <!--tiwtter-->
            <div class="col-md-4">
              <div class="footer-info"></div>
            </div>
            <!--footer social links-->
            <div class="col-md-4">
              <div class="footer-social">
                <h3>Find us</h3>
                <ul>
                  <li><a href="<?= (isset($meta[ 'facebook' ][ 'value' ])) ? $meta[ 'facebook' ][ 'value' ] : ''; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="<?= (isset($meta[ 'twitter' ][ 'value' ])) ? $meta[ 'twitter' ][ 'value' ] : ''; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="<?= (isset($meta[ 'instagram' ][ 'value' ])) ? $meta[ 'instagram' ][ 'value' ] : ''; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="<?= (isset($meta[ 'pintrest' ][ 'value' ])) ? $meta[ 'pintrest' ][ 'value' ] : ''; ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                  <li><a href="<?= (isset($meta[ 'tumbler' ][ 'value' ])) ? $meta[ 'tumbler' ][ 'value' ] : '#'; ?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
                </ul>
              </div>
            </div>
            <!--subscribe form-->
            <div class="col-md-4">
              <div class="footer-info"></div>
            </div>
          </div>
          <div class="bold-separator">
            <span></span>
          </div>
          <!--footer contacts links -->
          <ul class="footer-contacts">
            <li><a href="#"><?= (isset($meta[ 'phone' ][ 'value' ])) ? $meta[ 'phone' ][ 'value' ] : ''; ?></a></li>
            <li><a href="#"><?= (isset($meta[ 'address' ][ 'value' ])) ? $meta[ 'address' ][ 'value' ] : ''; ?></a></li>
            <li><a href="#"><?= (isset($meta[ 'email' ][ 'value' ])) ? $meta[ 'email' ][ 'value' ] : ''; ?></a></li>
          </ul>
        </div>
      </div>
      <!--to top / privacy policy-->
      <div class="to-top-holder">
        <div class="container">
          <p><span> &#169; pumenu <?= date('Y') ?> . </span> All rights reserved.</p>
          <div class="to-top"><span>Back To Top </span><i class="fa fa-angle-double-up"></i></div>
        </div>
      </div>
    </footer>
  </div>
</div>

<!--footer starts from here-->

<!-- Initializing the slider -->
<script type="text/javascript">
		
		$(document).ready(function () {
				
				$('#slider').layerSlider({
						sliderVersion : '6.2.1' ,
						skin : 'v6' ,
						showCircleTimer : false ,
						skinsPath : '{{ asset('slider/layerslider/skins/') }}'
				});
				
				setInterval(function () {
						$('html').find('.btnClicker').next('a').attr('href' , "{{ action('frontEndController@gmap') }}").removeAttr('target');
				} , 200);
				
		});

</script>
<link rel="stylesheet" href="{{ asset('admin-assets/libs/jquery/select2/dist/css/select2.min.css?'.time()) }}">
<script src="{{ asset('admin-assets/libs/jquery/select2/dist/js/select2.full.min.js?'.time()) }}"></script>
<script>
		$(function () {
				$(".select2").select2();
				$(".select2").on('change' , function () {
						window.location.href = $(this).val();
				});
				
		});
		
		function autocomplete(inp , arr) {
				/*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
				var currentFocus;
				/*execute a function when someone writes in the text field:*/
				inp.addEventListener("input" , function (e) {
						var a , b , i , val = this.value;
						/*close any already open lists of autocompleted values*/
						closeAllLists();
						if (!val) {
								return false;
						}
						currentFocus = -1;
						/*create a DIV element that will contain the items (values):*/
						a = document.createElement("DIV");
						a.setAttribute("id" , this.id + "autocomplete-list");
						a.setAttribute("class" , "autocomplete-items");
						/*append the DIV element as a child of the autocomplete container:*/
						this.parentNode.appendChild(a);
						/*for each item in the array...*/
						for (i = 0; i < arr.length; i++) {
								/*check if the item starts with the same letters as the text field value:*/
								if (arr[i].substr(0 , val.length).toUpperCase() == val.toUpperCase()) {
										/*create a DIV element for each matching element:*/
										b = document.createElement("DIV");
										/*make the matching letters bold:*/
										b.innerHTML = "<strong>" + arr[i].substr(0 , val.length) + "</strong>";
										b.innerHTML += arr[i].substr(val.length);
										/*insert a input field that will hold the current array item's value:*/
										b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
										/*execute a function when someone clicks on the item value (DIV element):*/
										b.addEventListener("click" , function (e) {
												/*insert the value for the autocomplete text field:*/
												inp.value = this.getElementsByTagName("input")[0].value;
												/*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
												closeAllLists();
										});
										a.appendChild(b);
								}
						}
				});
				/*execute a function presses a key on the keyboard:*/
				inp.addEventListener("keydown" , function (e) {
						var x = document.getElementById(this.id + "autocomplete-list");
						if (x) x = x.getElementsByTagName("div");
						if (e.keyCode == 40) {
								/*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
								currentFocus++;
								/*and and make the current item more visible:*/
								addActive(x);
						} else if (e.keyCode == 38) { //up
								/*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
								currentFocus--;
								/*and and make the current item more visible:*/
								addActive(x);
						} else if (e.keyCode == 13) {
								/*If the ENTER key is pressed, prevent the form from being submitted,*/
								e.preventDefault();
								if (currentFocus > -1) {
										/*and simulate a click on the "active" item:*/
										if (x) x[currentFocus].click();
								}
						}
				});
				
				function addActive(x) {
						/*a function to classify an item as "active":*/
						if (!x) return false;
						/*start by removing the "active" class on all items:*/
						removeActive(x);
						if (currentFocus >= x.length) currentFocus = 0;
						if (currentFocus < 0) currentFocus = (x.length - 1);
						/*add class "autocomplete-active":*/
						x[currentFocus].classList.add("autocomplete-active");
				}
				
				function removeActive(x) {
						/*a function to remove the "active" class from all autocomplete items:*/
						for (var i = 0; i < x.length; i++) {
								x[i].classList.remove("autocomplete-active");
						}
				}
				
				function closeAllLists(elmnt) {
						/*close all autocomplete lists in the document,
            except the one passed as an argument:*/
						var x = document.getElementsByClassName("autocomplete-items");
						for (var i = 0; i < x.length; i++) {
								if (elmnt != x[i] && elmnt != inp) {
										x[i].parentNode.removeChild(x[i]);
								}
						}
				}
				
				/*execute a function when someone clicks in the document:*/
				document.addEventListener("click" , function (e) {
						closeAllLists(e.target);
				});
		}
		
		/*An array containing all the country names in the world:*/
		var countries = [
      <?php if(isset($data) && count($data) > 0){
      foreach($data as $d){
        echo "'$d->name',";
      }
    } ?>
		];
		var replicator = '<?= action('frontEndController@index', ['id' => 111]) ?>';
		var links = '<?= json_encode($data) ?>';
		
		$("#btnSearch").on('click' , function () {
				var search = JSON.parse(links);
				$.each(search , function (key , val) {
						if ($("#myInput").val() == val.name) {
								var url = replicator.replace('/111' , '/' + val.name);
								window.location.href = url;
						}
				})
		});
		
		/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
		
		autocomplete(document.getElementById("myInput") , countries);
		
		
		/*handling seatch */

</script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyD5H5DwCVRwToK6BAHjjor3qNj1Taj5JUc"></script>

<!--=============== scripts  ===============-->
<script type="text/javascript" src={{ asset('front/js/plugins.js') }}></script>
<script type="text/javascript" src={{ asset('front/js/scripts.js') }}></script>
<script>
		$(function () {
      <?php if(isset($meta[ 'lang' ]) && $meta[ 'lang' ] == 'he'){ ?> 
        
				setTimeout(function(){$('html').find(".language-changer  .il").trigger('click');} , 2000);
				
      <?php } ?>
			$(".language-changer  a").on('click' , function (event) {
					if ($(this).attr('href') == '#') {
							event.preventDefault();
							var lang = $(this).attr('class');
							if (lang == 'en') {
									$("[data-langen]").each(function (index , val) {
											$(this).html($(this).attr('data-langen'));
									});
							} else {
									$("[data-langhe]").each(function (index , val) {
											$(this).html($(this).attr('data-langhe'));
									});
							}
					}
			});
		});
</script>
</body>
</html>