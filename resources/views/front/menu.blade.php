@extends('front.partials.mainLayout-front')
@section('content')
  <!--=============== Hero content ===============-->
  <style>
    .description-small {
      display: inline-block;
      width: 100%;
      text-align: left;
    }

    /*.menu-item-price .contamper {float: right;}*/

    .menu-item-price img {}

    .pleaser {text-align: right; }

    .contacter > section {
      padding: 20px 0 !important;
      margin-bottom: 35px;
    }

    .blanker {padding: 50px 0px;text-align: center;}

    .image-popup.title {
      max-width: 200px;
      display: inline-block;
      white-space: normal;
      line-height: 23px;
      font-size: 0.9em;}

    .parallax-section .overlay {
      opacity: 0.9 !important;
    }

    .parallax-section.header-section {
      display: none;
    }
  </style>
  <!--hero end-->
  <div class="content ">

    <section st class="parallax-section header-section">
      <div class="bg bg-parallax" style="background-image:url({{ asset('front/images/bg/1.jpg') }})" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
      <div class="overlay"></div>
      <div class="container">
        <h2>Hot dishes</h2>
        <h3><?= (isset($setting->title)) ? $setting->title : ''; ?></h3>
      </div>
    </section>

    <div class="col-lg-12" style="background-color: #191919;">
      <img src="{{ asset("images/popup_menu_final_white-01.png") }}" alt="Logo" class="image-responsive" style="max-width: 250px; padding: 35px; margin-top: 18px;">
    </div>
    <?php if(count($dishes) < 1 && count($drinks) < 1 && count($specialMenu) < 1 ){ ?>
    <section>
      <h3 class="blanker"> Sorry this resturent does not have any menu yet . please try again later</h3>
    </section>
    <?php } ?>
    <div class="contacter">
      <?php if(isset($dishes) && count($dishes) > 0 ){ ?>
      <section>
        <!-- <div class="triangle-decor"></div> -->
        <div class="menu-bg lbd" style="background-image:url({{ asset('front/images/menu/1.png') }})" data-top-bottom="transform: translateX(200px);" data-bottom-top="transform: translateX(-200px);">
        </div>
        <div class="container">
          <div class="separator color-separator"></div>
          <h2>Menu</h2>
          <div class="menu-holder">
            <div class="row">
              <?php if(count($dishes) > 0){
              foreach($dishes as $d){
              ?>
              <div class="col-md-6" style="margin-top:10px;display: inline-block;">
                <!--menu item-->
                <div class="menu-item  hot-deal">
                  <span class="hot-desc">Featured</span>
                  <div class="menu-item-details">
                    <div class="menu-item-desc">

                      <a href="#" class="image-popup title" data-langen="<?= $d->name.' $ '.$d->price ?>" data-langhe="<?= $d->name_he.' $ '.$d->price ?>"> <?= $d->name.' $ '.$d->price ?></a>
                    </div>
                    {{--<div class="menu-item-dot"></div>--}}
                    <div class="menu-item-prices">
                      <div class="menu-item-price pleaser">
                      <span class="contamper">
                        <img class="img-circle img-thumbnail img-responsive" src="<?= asset('storage/uploads/'.$d->image) ?>" alt="Image not available" style=" width: 115px; border-radius: 5%; height: 69px; margin-top: 10px; ">
                      </span>
                      </div>
                    </div>
                  </div>
                  <small class="description-small" data-langen="<?= $d->description ?>" data-langhe="<?= $d->description_he ?>"><?= $d->description ?></small>
                  <p class="description-detail" data-langen="<?= strip_tags($d->detail_description) ?>" data-langhe="<?= strip_tags($d->detail_description_he) ?>"><?= strip_tags($d->detail_description) ?></p>
                </div>
                <!--menu item end-->
              </div>
              <?php }
              } ?>
            </div>
          </div>
          <div class="bold-separator">
            <span></span>
          </div>
        </div>
      </section>
      <?php } ?>
      
      <?php if(isset($specialMenu) && count($specialMenu) > 0 ){ ?>
      <section>
        <!-- <div class="triangle-decor"></div> -->
        <div class="menu-bg lbd" style="background-image:url({{ asset('front/images/menu/1.png') }})" data-top-bottom="transform: translateX(200px);" data-bottom-top="transform: translateX(-200px);">
        </div>
        <div class="container">
          <div class="separator color-separator"></div>
          <h2>Special Menu</h2>
          <div class="menu-holder">
            <div class="row">
              <?php if(count($specialMenu) > 0){
              foreach($specialMenu as $d){
              ?>
              <div class="col-md-6" style="margin-top:10px;display: inline-block;">
                <!--menu item-->
                <div class="menu-item  hot-deal">
                  <span class="hot-desc">Featured</span>
                  <div class="menu-item-details">
                    <div class="menu-item-desc">
                      <a href="#" class="image-popup title" data-langen="<?= $d->name.' $ '.$d->price ?>" data-langhe="<?= $d->name_he.' $ '.$d->price ?>"><?= $d->name.' $ '.$d->price ?></a>
                    </div>
                    {{--<div class="menu-item-dot"></div>--}}
                    <div class="menu-item-prices">
                      <div class="menu-item-price pleaser">
                      <span class="contamper">
                        <img class="img-circle img-thumbnail img-responsive" src="<?= asset('storage/uploads/'.$d->image) ?>" alt="Image not available" style=" width: 115px; border-radius: 5%; height: 69px; margin-top: 10px; ">
                      </span>
                      </div>
                    </div>
                  </div>
                  <small class="description-small" data-langen="<?= $d->description ?>" data-langhe="<?= $d->description_he ?>"><?= $d->description ?></small>
                  <p class="description-detail" data-langen="<?= strip_tags($d->detail_description) ?>" data-langhe="<?= strip_tags($d->detail_description_he) ?>"><?= strip_tags($d->detail_description) ?></p>
                </div>
                <!--menu item end-->
              </div>
              <?php }
              } ?>
            </div>
          </div>
          <div class="bold-separator">
            <span></span>
          </div>
        </div>
      </section>
      <?php } ?>
      <?php if(isset($drinks) && count($drinks) > 0 ){ ?>
      <section>
        <!-- <div class="triangle-decor"></div> -->
        <div class="menu-bg lbd" style="background-image:url({{ asset('front/images/menu/1.png') }})" data-top-bottom="transform: translateX(200px);" data-bottom-top="transform: translateX(-200px);">
        </div>
        <div class="container">
          <div class="separator color-separator"></div>
          <h2>Drinks</h2>
          <div class="menu-holder">
            <div class="row">
              <?php if(count($drinks) > 0){
              foreach($drinks as $d){
              ?>
              <div class="col-md-6" style="margin-top:10px;display: inline-block;">
                <!--menu item-->
                <div class="menu-item  hot-deal">
                  <span class="hot-desc">Featured</span>
                  <div class="menu-item-details">
                    <div class="menu-item-desc">
                      <a href="#" class="image-popup title" data-langen="<?= $d->name.' $ '.$d->price ?>" data-langhe="<?= $d->name_he.' $ '.$d->price ?>"><?= $d->name.' $ '.$d->price ?></a>
                    </div>
                    {{--<div class="menu-item-dot"></div>--}}
                    <div class="menu-item-prices">
                      <div class="menu-item-price pleaser">
                      <span class="contamper">
                        <img class="img-circle img-thumbnail img-responsive" src="<?= asset('storage/uploads/'.$d->image) ?>" alt="Image not available" style=" width: 115px; border-radius: 5%; height: 69px; margin-top: 10px; ">
                      </span>
                      </div>
                    </div>
                  </div>
                  <small class="description-small" data-langen="<?= $d->description ?>" data-langhe="<?= $d->description_he ?>"><?= $d->description ?></small>
                  <p class="description-detail" data-langen="<?= strip_tags($d->detail_description) ?>" data-langhe="<?= strip_tags($d->detail_description_he) ?>"><?= strip_tags($d->detail_description) ?></p>
                </div>
                <!--menu item end-->
              </div>
              <?php }
              } ?>
            </div>
          </div>
          <div class="bold-separator">
            <span></span>
          </div>
        </div>
      </section>
      <?php } ?>
    </div>
  </div>
  <!--content end-->
@endsection()